package com.nekitdev.jetpacknavigation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.nekitdev.jetpacknavigation.R
import kotlinx.android.synthetic.main.fragment_c.*

class CFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_c, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_nav_to_a.setOnClickListener { findNavController(this).navigate(R.id.action_c_to_a, null) }
    }

}
