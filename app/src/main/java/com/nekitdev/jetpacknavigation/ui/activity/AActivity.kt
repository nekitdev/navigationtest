package com.nekitdev.jetpacknavigation.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nekitdev.jetpacknavigation.R

class AActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a)
    }
}
